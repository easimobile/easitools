package com.easipos.easitools;

import android.widget.EditText;
import android.widget.TextView;

import com.symbol.emdk.EMDKManager;
import com.symbol.emdk.EMDKResults;
import com.symbol.emdk.barcode.BarcodeManager;
import com.symbol.emdk.barcode.Scanner;
import com.symbol.emdk.barcode.ScannerInfo;

import java.util.ArrayList;
import java.util.List;

public class Global {

    public static EMDKResults results;
    public static EMDKManager emdkManager = null;
    public static  List<ScannerInfo> scannerList = new ArrayList<ScannerInfo>();
    public static  BarcodeManager barcodeManager = null;
    public static  Scanner scanner = null;
    public static int defaultScannerIndex;
    public static TextView productCodeName;
    public static boolean bSoftTriggerSelected = false;
    public static InventoryLookupResultAdapter inventoryResultAdapter;
    public static  List<InventoryLookupResultEntry> inventoryResults = new ArrayList<>();
    public static CustomProgressDialog myCheckingProgressDialog;
    public static MainMenuActivity.InventoryLookupTask iLookupTask;
    public static InventoryLookupFragment.InventoryLookupTask iLookupTaskFragment;
    public static EditText productCodeNameField;
    public static int appLayer = 1;
}
