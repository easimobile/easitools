package com.easipos.easitools;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.symbol.emdk.EMDKManager;
import com.symbol.emdk.EMDKResults;
import com.symbol.emdk.barcode.BarcodeManager;
import com.symbol.emdk.barcode.ScanDataCollection;
import com.symbol.emdk.barcode.Scanner;
import com.symbol.emdk.barcode.ScannerConfig;
import com.symbol.emdk.barcode.ScannerException;
import com.symbol.emdk.barcode.ScannerInfo;
import com.symbol.emdk.barcode.ScannerResults;
import com.symbol.emdk.barcode.StatusData;

import java.util.ArrayList;
import java.util.List;

public class InventoryLookupFragment extends Fragment{


    private RecyclerView inventoryLookupResultRecycleView;
    private CustomProgressDialog progressDialog;
    private Activity applicationActivity;
    private Button scanButton;
    private Button lookupButton;


    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.fragment_inventory_lookup, container, false);
        inventoryLookupResultRecycleView = view.findViewById(R.id.lookupResultRecyclerView);
        Global.inventoryResults.clear();

        Global.inventoryResults.add(new InventoryLookupResultEntry("** Location **","** Count **","** Price **"));
        if (Global.inventoryResultAdapter == null)
            Global.inventoryResultAdapter = new InventoryLookupResultAdapter(Global.inventoryResults);

        inventoryLookupResultRecycleView.setAdapter(Global.inventoryResultAdapter);
        inventoryLookupResultRecycleView.setLayoutManager(new LinearLayoutManager(getContext()));

        Global.productCodeName = view.findViewById(R.id.productCodeNameLabel);
        Global.productCodeName.setText("");
        Global.productCodeNameField = view.findViewById(R.id.inputCodeField);
        Global.productCodeNameField.setText("");
        scanButton = view.findViewById(R.id.scan);
        lookupButton = view.findViewById(R.id.lookup);

        scanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scanButton.setEnabled(false);
                startScan();
                scanButton.setEnabled(true);
            }
        });

        lookupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String data = Global.productCodeNameField.getText().toString().trim();
                if(data.length()!=0) {
                    scanButton.setEnabled(false);

                    Global.productCodeName.setText("");
                    for( int i = Global.inventoryResultAdapter.getItemCount() - 1 ; i > 0 ; i-- )
                        Global.inventoryResultAdapter.remove(i);

                    Global.iLookupTaskFragment = new InventoryLookupTask(data);
                    Global.iLookupTaskFragment.execute();
                    scanButton.setEnabled(true);
                }
            }
        });

        return view;
    }

    public void setApplicationContext(Activity applicationActivity)
    {
        this.applicationActivity = applicationActivity;
    }

    private void startScan(){
        Global.bSoftTriggerSelected=true;
        cancelRead();
    }

    private void cancelRead(){
        if (Global.scanner != null) {
            if (Global.scanner.isReadPending()) {
                try {
                    Global.scanner.cancelRead();
                } catch (ScannerException e) {
                    //updateStatus(e.getMessage());
                }
            }
        }
    }

    public class InventoryLookupTask extends AsyncTask<Void, String, Boolean> {
        private String code;
        SOAPWeb.SOAPReturnInventoryLookup inventoryLookupWebResult;

        InventoryLookupTask(String code) {
            this.code = code;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.M)
        @Override
        protected Boolean doInBackground(Void... a) {
            SOAPWeb soap = new SOAPWeb(getContext());

            applicationActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Global.myCheckingProgressDialog.show();
                }
            });

            inventoryLookupWebResult = soap.lookupInventoryCount(code);

            if(inventoryLookupWebResult.getResult().equals("True"))
                return true;
            else
                return false;
        }

        @TargetApi(Build.VERSION_CODES.M)
        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            if(result)
            {
                final String productCode = inventoryLookupWebResult.getProductCode();
                final String productName = inventoryLookupWebResult.getProductName();
                final List<InventoryLookupResultEntry> countResultByLocation = inventoryLookupWebResult.getInventoryLookupResultEntry();

                getActivity().runOnUiThread(new Runnable(){
                    @Override
                    public void run() {
                        Global.productCodeNameField.setText("");
                        Global.productCodeName.setText(productCode + " - " + productName);
                        for ( int i = 0 ; i < countResultByLocation.size() ; i++)
                        {
                            Global.inventoryResultAdapter.insert(Global.inventoryResultAdapter.getItemCount(),countResultByLocation.get(i));
                        }
                    }
                });
                Global.myCheckingProgressDialog.cancel();
            }
            else {
                Global.myCheckingProgressDialog.cancel();
                Toast.makeText(getContext(), inventoryLookupWebResult.getMessage(), Toast.LENGTH_SHORT).show();
            }

            Global.iLookupTask = null;
        }

        @Override
        protected void onCancelled() {
            Global.iLookupTask = null;
            super.onCancelled();
        }
    }
}
