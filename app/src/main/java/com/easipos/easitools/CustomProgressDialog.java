package com.easipos.easitools;

import android.app.ProgressDialog;
import android.content.Context;

public class CustomProgressDialog extends ProgressDialog{

    /**
     * Custom Dialog to be shown when sending commands to the RFID Reader
     */

    private static final String MESSAGE = "Saving Settings....";

    /**
     * Constructor to handle the initialization
     *
     * @param context - Context to be used
     */
    public CustomProgressDialog(Context context, String message) {
        super(context, ProgressDialog.STYLE_SPINNER);
        setTitle(null);
        if (message != null)
            setMessage(message);
        else
            setMessage(MESSAGE);
        setCancelable(false);
    }
}
