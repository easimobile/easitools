package com.easipos.easitools;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.symbol.emdk.EMDKManager;
import com.symbol.emdk.EMDKResults;
import com.symbol.emdk.barcode.BarcodeManager;
import com.symbol.emdk.barcode.ScanDataCollection;
import com.symbol.emdk.barcode.Scanner;
import com.symbol.emdk.barcode.ScannerConfig;
import com.symbol.emdk.barcode.ScannerException;
import com.symbol.emdk.barcode.ScannerInfo;
import com.symbol.emdk.barcode.ScannerResults;
import com.symbol.emdk.barcode.StatusData;

import java.util.ArrayList;
import java.util.List;

public class MainMenuActivity extends AppCompatActivity implements EMDKManager.EMDKListener, Scanner.DataListener, Scanner.StatusListener, BarcodeManager.ScannerConnectionListener {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
        setContentView(R.layout.activity_main_menu);

        if (Global.myCheckingProgressDialog == null)
            Global.myCheckingProgressDialog = new CustomProgressDialog(this,"Checking..");

        Global.results = EMDKManager.getEMDKManager(getApplicationContext(), this);
        if (Global.results.statusCode != EMDKResults.STATUS_CODE.SUCCESS) {
            Toast.makeText(getApplicationContext(), "Scanner hardware failed! Please restart device.", Toast.LENGTH_SHORT).show();
        }

    }
    @Override
    protected void onResume() {
        super.onResume();
        // The application is in foreground
        if (Global.emdkManager != null) {
            // Acquire the barcode manager resources
            initBarcodeManager();
            // Initialize scanner
            initScanner();
        }
    }
    @Override
    protected void onPause() {
        super.onPause();
        // The application is in background
        // Release the barcode manager resources
        deInitScanner();
        deInitBarcodeManager();
    }

    @Override
    public void onBackPressed()
    {
        if(Global.appLayer == 2)
        {
            Global.appLayer = 1;
            super.onBackPressed();
        }
        else if(Global.appLayer == 1)
        {
            deInitScanner();
            deInitBarcodeManager();
            finish();
            System.exit(0);
        }
    }

    public void initScanner() {
        if (Global.scanner == null) {
            if ((Global.scannerList != null) && (Global.scannerList.size() != 0)) {
                if (Global.barcodeManager != null)
                    Global.scanner = Global.barcodeManager.getDevice(Global.scannerList.get(Global.defaultScannerIndex));
            }
            else {
                Toast.makeText(getApplicationContext(), "\"Failed to get the specified scanner device! Please close and restart the application.", Toast.LENGTH_SHORT).show();
                return;
            }
            if (Global.scanner != null) {
                Global.scanner.addDataListener(this);
                Global.scanner.addStatusListener(this);
                try {
                    Global.scanner.enable();
                } catch (ScannerException e) {
                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    deInitScanner();
                }
            }else{
                Toast.makeText(getApplicationContext(), "Failed to initialize the scanner device.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void deInitScanner() {
        if (Global.scanner != null) {
            try{
                Global.scanner.disable();
                Global.scanner.release();
            } catch (Exception e) {
                Log.d("Exception",e.getMessage());
            }
            Global.scanner = null;
        }
    }

    public void initBarcodeManager(){
        Global.barcodeManager = (BarcodeManager) Global.emdkManager.getInstance(EMDKManager.FEATURE_TYPE.BARCODE);
        // Add connection listener
        if (Global.barcodeManager != null) {
            Global.barcodeManager.addConnectionListener(this);
        }
    }

    public void deInitBarcodeManager(){
        if (Global.emdkManager != null) {
            Global.emdkManager.release(EMDKManager.FEATURE_TYPE.BARCODE);
        }
    }

    private void setDecoders() {
        if (Global.scanner != null) {
            try {
                ScannerConfig config = Global.scanner.getConfig();
                // Set EAN8
                config.decoderParams.ean8.enabled = true;
                // Set EAN13
                config.decoderParams.ean13.enabled = true;
                // Set Code39
                config.decoderParams.code39.enabled= true;
                //Set Code128
                config.decoderParams.code128.enabled = true;
                Global.scanner.setConfig(config);
            } catch (ScannerException e) {
                Log.d("Exception",e.getMessage());
            }
        }
    }

    @Override
    public void onClosed() {
        deInitScanner();
        deInitBarcodeManager();
        if (Global.emdkManager != null) {
            Global.emdkManager.release();
            Global.emdkManager = null;
        }
    }

    @Override
    public void onConnectionChange(ScannerInfo scannerInfo, BarcodeManager.ConnectionState connectionState) {
        Log.d("Scanner","onConnectionChange");
    }

    @Override
    public void onData(ScanDataCollection scanDataCollection) {
        if ((scanDataCollection != null) && (scanDataCollection.getResult() == ScannerResults.SUCCESS)) {
            ArrayList<ScanDataCollection.ScanData> scanData = scanDataCollection.getScanData();
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Global.productCodeName.setText("");
                    for( int i = Global.inventoryResultAdapter.getItemCount() - 1 ; i > 0 ; i-- )
                        Global.inventoryResultAdapter.remove(i);
                }
            });
            for(ScanDataCollection.ScanData data : scanData) {
                Global.iLookupTask = new InventoryLookupTask(data.getData());
                Global.iLookupTask.execute();
            }
        }
    }

    @Override
    public void onStatus(StatusData statusData) {
        StatusData.ScannerStates state = statusData.getState();
        switch(state) {
            case IDLE:
                Log.d("Scanner-Zebra","onStatus-Idle");
                // set trigger type
                if(Global.bSoftTriggerSelected) {
                    Global.scanner.triggerType = Scanner.TriggerType.SOFT_ONCE;
                    Global.bSoftTriggerSelected = false;
                } else {
                    Global.scanner.triggerType = Scanner.TriggerType.HARD;
                }
                setDecoders();
                // submit read
                if(!Global.scanner.isReadPending() && Global.scanner!= null) {
                    try {
                        Global.scanner.read();
                        Log.d("Scanner-Zebra","scanner.read()");
                    } catch (ScannerException e) {
                        Log.d("Scanner",e.getMessage());
                    }
                }
                break;
            case WAITING:
                Log.d("Scanner-Zebra","onStatus-Waiting");
                break;
            case SCANNING:

                break;
            case DISABLED:

                break;
            case ERROR:

                break;
            default:
                break;
        }
    }

    @Override
    public void onOpened(EMDKManager emdkManager) {

        Global.emdkManager = emdkManager;

        initBarcodeManager();

        // Enumerate scanner devices
        Global.scannerList = Global.barcodeManager.getSupportedDevicesInfo();

        if ((Global.scannerList != null) && (Global.scannerList.size() != 0)) {
            for(int i = 0 ; i<Global.scannerList.size() ; i++)
            {
                if(Global.scannerList.get(i).isDefaultScanner()) {
                    Global.defaultScannerIndex = i;
                    break;
                }
            }
        }

        deInitScanner();
        initScanner();

        // Set default scanner
        //spinnerScannerDevices.setSelection(defaultIndex);
    }

    public void openInventoryLookupFragment(View v){
        Fragment myfragment;
        myfragment = new InventoryLookupFragment();
        ((InventoryLookupFragment) myfragment).setApplicationContext(this);
        FragmentManager fm = getFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.mainForm, myfragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
        Global.appLayer = 2;
    }

    public class InventoryLookupTask extends AsyncTask<Void, String, Boolean> {
        private String code;
        SOAPWeb.SOAPReturnInventoryLookup inventoryLookupWebResult;

        InventoryLookupTask(String code) {
            this.code = code;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Boolean doInBackground(Void... a) {
            SOAPWeb soap = new SOAPWeb(getBaseContext());
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Global.myCheckingProgressDialog.show();
                }
            });

            inventoryLookupWebResult = soap.lookupInventoryCount(code);

            if(inventoryLookupWebResult.getResult().equals("True"))
                return true;
            else
                return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            if(result)
            {
                final String productCode = inventoryLookupWebResult.getProductCode();
                final String productName = inventoryLookupWebResult.getProductName();
                final List<InventoryLookupResultEntry> countResultByLocation = inventoryLookupWebResult.getInventoryLookupResultEntry();

                runOnUiThread(new Runnable(){
                    @Override
                    public void run() {
                        Global.productCodeNameField.setText("");
                        Global.productCodeName.setText(productCode + " - " + productName);
                        for ( int i = 0 ; i < countResultByLocation.size() ; i++)
                        {
                            Global.inventoryResultAdapter.insert(Global.inventoryResultAdapter.getItemCount(),countResultByLocation.get(i));
                        }
                    }
                });
                Global.myCheckingProgressDialog.cancel();
            }
            else {
                Global.myCheckingProgressDialog.cancel();
                Toast.makeText(MainMenuActivity.this, inventoryLookupWebResult.getMessage(), Toast.LENGTH_SHORT).show();
            }

            Global.iLookupTask = null;
        }

        @Override
        protected void onCancelled() {
            Global.iLookupTask = null;
            super.onCancelled();
        }
    }

    public void openSettingsPage(View view) {
        // TableLayout settingsTable = (TableLayout) findViewById(R.id.settings_Table);
        View v = view;
        Intent intent = new Intent(MainMenuActivity.this, SettingsActivity.class);
        startActivity(intent);
    }
}
