package com.easipos.easitools;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

public class InventoryResultHolder extends RecyclerView.ViewHolder {

    TextView stockLocation;
    TextView inventoryCount;
    TextView price;

    InventoryResultHolder(View itemView){
        super(itemView);
        stockLocation = (TextView)itemView.findViewById(R.id.stockLocation);
        inventoryCount = (TextView)itemView.findViewById(R.id.inventoryCount);
        price = (TextView)itemView.findViewById(R.id.price);
    }

}
