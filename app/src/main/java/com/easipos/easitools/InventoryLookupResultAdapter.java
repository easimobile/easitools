package com.easipos.easitools;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Collections;
import java.util.List;

public class InventoryLookupResultAdapter extends RecyclerView.Adapter<InventoryResultHolder> {

    List<InventoryLookupResultEntry> list = Collections.emptyList();

    public InventoryLookupResultAdapter(List<InventoryLookupResultEntry>list){this.list=list;}

    @NonNull
    @Override
    public InventoryResultHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.inventory_lookup_result_row_layout, viewGroup, false);
        InventoryResultHolder holder = new InventoryResultHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull InventoryResultHolder inventoryResultHolder, int i) {
        inventoryResultHolder.stockLocation.setText(list.get(i).getStkLocation());
        inventoryResultHolder.inventoryCount.setText(list.get(i).getInventoryCount());
        inventoryResultHolder.price.setText(list.get(i).getPrice());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    // Insert a new item to the RecyclerView on a predefined position
    public void insert(int position, InventoryLookupResultEntry data) {
        list.add(position, data);
        notifyItemInserted(position);
    }

    // Remove a RecyclerView item containing a specified Data object
    public void remove(int position) {
        list.remove(position);
        notifyItemRemoved(position);
    }

}
