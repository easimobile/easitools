package com.easipos.easitools;

public class InventoryLookupResultEntry {

    private String stkLocation;
    private String inventoryCount;
    private String price;

    InventoryLookupResultEntry(String stkLocation, String inventoryCount, String price)
    {
        this.stkLocation = stkLocation;
        this.inventoryCount = inventoryCount;
        this.price = price;
    }

    public String getStkLocation(){return this.stkLocation;}
    public String getInventoryCount(){return this.inventoryCount;}
    public String getPrice(){return this.price;}
}
