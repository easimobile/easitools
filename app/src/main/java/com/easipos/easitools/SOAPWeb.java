package com.easipos.easitools;

import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpResponseException;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

public class SOAPWeb {
    private String SOAP_ACTION;
    private String METHOD_NAME;
    private static String NAMESPACE = "http://tempuri.org/";
    private String webServiceURL;
    private Context context;
    private String companyCode;

    public SOAPWeb(Context context) {
        this.context = context;
        SharedPreferences SP = PreferenceManager.getDefaultSharedPreferences(context);
        webServiceURL = SP.getString("webServiceURL", "NA");
        companyCode = SP.getString("companyCode", "NA");
        SOAP_ACTION = NAMESPACE + METHOD_NAME;
    }

    public SOAPReturnInventoryLookup lookupInventoryCount (String code) {
        METHOD_NAME = "StockQuery";
        SOAP_ACTION = NAMESPACE + METHOD_NAME;

        SOAPReturnInventoryLookup soapReturnResult;

        PropertyInfo pi = new PropertyInfo();
        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);

        SoapObject item = new SoapObject(null,"request");

        pi.setName("Code");
        pi.setValue(code);
        pi.setType(String.class);

        item.addProperty(pi);

        pi = new PropertyInfo();
        pi.setName("CompanyCode");
        pi.setValue(companyCode);
        pi.setType(String.class);

        item.addProperty(pi);

        request.addSoapObject(item);

        envelope.implicitTypes = true;
        envelope.setOutputSoapObject(request);
        envelope.dotNet = true;

        HttpTransportSE httpTransport = new HttpTransportSE(webServiceURL);
        httpTransport.debug = true;
        try {
            httpTransport.call(SOAP_ACTION, envelope);
            Log.d("Dump",httpTransport.requestDump);
            Log.d("Dump",httpTransport.responseDump);
        } catch (HttpResponseException e) {
            // TODO Auto-generated catch block
            Log.e("HttpResponseException", e.getMessage());
            e.printStackTrace();
            soapReturnResult = new SOAPReturnInventoryLookup("False",e.getMessage(),null,null,null);
            return soapReturnResult;
        } catch (IOException e) {
            // TODO Auto-generated catch block
            Log.e("IOException", e.getMessage());
            e.printStackTrace();
            soapReturnResult = new SOAPReturnInventoryLookup("False",e.getMessage(),null,null,null);
            return soapReturnResult;
        } catch (XmlPullParserException e) {
            // TODO Auto-generated catch block
            Log.e("XmlPullParserException", e.getMessage());
            e.printStackTrace();
            soapReturnResult = new SOAPReturnInventoryLookup("False",e.getMessage(),null,null,null);
            return soapReturnResult;
        } //send request

        SoapObject response = null;
        try {
            response = (SoapObject) envelope.bodyIn;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e("SOAP_SoapFault", e.getMessage());
            e.printStackTrace();
            soapReturnResult = new SOAPReturnInventoryLookup("False",e.getMessage(),null,null,null);
            return soapReturnResult;
        }

        SoapObject result = (SoapObject) response.getProperty(0);
        String returnedResult = String.valueOf(result);
        if(returnedResult.equals("anyType{LocQty=anyType{}; }")){
            soapReturnResult = new SOAPReturnInventoryLookup("False","No result.",null,null,null);
            return soapReturnResult;
        }

        String status = String.valueOf(result.getProperty("status")).trim();

        if (status.trim().equals("False")) {
            soapReturnResult = new SOAPReturnInventoryLookup("False","Product not found.",null,null,null);
            return soapReturnResult;
        }

        String prodCode = String.valueOf(result.getProperty("ProdCode")).trim();
        String prodName =  String.valueOf(result.getProperty("ProdName")).trim();

        SoapObject result2 = (SoapObject) result.getProperty("LocQty");
        List <InventoryLookupResultEntry> LocQty = new ArrayList<InventoryLookupResultEntry>();

        for (int i = 0 ; i < result2.getPropertyCount() ; i++)
        {
            SoapObject locQtyEntry = (SoapObject) result2.getProperty(i);
            String location = String.valueOf(locQtyEntry.getProperty(1));
            String quantity = String.valueOf(locQtyEntry.getProperty(2));
            String price = String.valueOf(locQtyEntry.getProperty(3));
            LocQty.add(new InventoryLookupResultEntry(location,quantity,price));
        }

        soapReturnResult = new SOAPReturnInventoryLookup(status,"",prodCode,prodName,LocQty);

        return soapReturnResult;
    }

    public class SOAPReturnInventoryLookup{
        private String result;
        private String message;
        private String productCode;
        private String productName;
        private List <InventoryLookupResultEntry> inventoryLookupResultEntry;

        SOAPReturnInventoryLookup(String result, String message, String productCode, String productName, List <InventoryLookupResultEntry> inventoryLookupResultEntry)
        {
            this.result =result;
            this.message=message;
            this.productCode = productCode;
            this.productName = productName;
            this.inventoryLookupResultEntry=inventoryLookupResultEntry;
        }

        public String getResult(){return this.result;}
        public String getMessage(){return this.message;}
        public String getProductCode(){return this.productCode;}
        public String getProductName(){return this.productName;}
        public List<InventoryLookupResultEntry> getInventoryLookupResultEntry(){return this.inventoryLookupResultEntry;}
    }

}
